<?php

// phpcs:ignoreFile
namespace Drupal\rfn_debug\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for rfn_debug routes.
 */
class RfnDebugController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $artist = \Drupal::service('entity_type.manager')->getStorage('node')->load(7268);
    $collection = \Drupal::service('entity_type.manager')->getStorage('node')->load(8899);
    $track = \Drupal::service('entity_type.manager')->getStorage('node')->load(9622);

    // 9622 = track
    \Drupal::messenger()->addError("TODO - WHEN MODIFYING EITHER TRACK OR COLLECTION, UPDATE TRACK's COLLECTIONS, AND COLLECTION'S MEDIA ITEMS - check for synch after each");

    \Drupal::messenger()->addMessage("Track we will be looking at is: {$track->getTitle()}");
    $track_collection = $track->collections();
    foreach ($track_collection as $tcollection) {
      \Drupal::messenger()->addMessage("TRACK COLLECTION: {$tcollection->getTitle()}");
    }

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works! : ' . $artist->getTitle()),
    ];

    return $build;
  }

}
